from django.db import models

from django.db import models

# Create your models here.

class usuario(models.Model):
    id_usuario=models.AutoField(primary_key=True) 
    nombre=models.CharField(max_length=30 ,blank=True, null=True)
    apellido=models.CharField( max_length=30,blank=True, null=True)
    direccion=models.CharField( max_length=50,blank=True, null=True)
    telefono=models.CharField( max_length=30,blank=True, null=True)
    tipo=models.CharField( max_length=30,blank=True, null=True)
    account=models.CharField(max_length=50,blank=True, null=True)
    password=models.CharField( max_length=20,blank=True, null=True)

  
class cliente(models.Model):
    id_cliente=models.AutoField(primary_key=True) 
    nombre=models.CharField(max_length=30)
    apellido=models.CharField( max_length=30,blank=True, null=True)
    direccion=models.CharField( max_length=50,blank=True, null=True)
    telefono=models.CharField( max_length=30,blank=True, null=True)

class venta(models.Model):
    id_venta=models.AutoField(primary_key=True) 
    fecha_venta=models.DateTimeField(blank=True)
    id_cliente=models.ForeignKey(cliente, on_delete=models.CASCADE)
    id_usuario=models.ForeignKey(usuario, on_delete=models.CASCADE)
        
class producto(models.Model):
    id_producto=models.AutoField(primary_key=True) 
    nombre=models.CharField( max_length=30,blank=True, null=True)
    descripcion=models.CharField( max_length=50,blank=True, null=True)
    precio=models.TextField()
    stock=models.TextField()  
class detalle(models.Model):   
    cantidad=models.IntegerField()
    precio=models.IntegerField()
    total=models.IntegerField()
    id_producto=models.ForeignKey(producto, on_delete=models.CASCADE)
    id_venta=models.ForeignKey(venta, on_delete=models.CASCADE)
   

